@FunctionalInterface
public interface Calculations {
    public abstract int calculate(int a, int b, int c);
}
