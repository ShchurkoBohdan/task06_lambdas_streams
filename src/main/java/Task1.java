import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Task1 {

    public static int getMaxValue(int i, int k, int n) {
        Calculations maxVal = (a, b, c) -> {
            ArrayList<Integer> list = new ArrayList<>();
            list.add(Integer.valueOf(a));
            list.add(Integer.valueOf(b));
            list.add(Integer.valueOf(c));
            return Collections.max(list);
        };
        return maxVal.calculate(i, k, n);
    }

    public static int getAvg(int i, int k, int n){
        Calculations getAvg = (a, b, c) -> (a + b + c) / 3;
        return getAvg.calculate(i, k, n);
    }
}
